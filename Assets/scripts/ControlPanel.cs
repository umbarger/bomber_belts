﻿using UnityEngine;
using System.Collections;

public class ControlPanel : MonoBehaviour {
	
	bool activated;
	ControlPanel opposite_panel;
	string opposite_name;

	// Use this for initialization
	void Start () 
	{
		activated = false;
		string tag = gameObject.tag;
		string name = gameObject.name;
		name = name.Remove (0,1);
		
		if ( tag == "left_control_panel")
			opposite_name = "r" + name;
		else
			opposite_name = "l" + name;
		
		opposite_panel = GameObject.Find(opposite_name).GetComponent<ControlPanel>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	
	public void Activate()
	{
		activated = true;
		opposite_panel.Deactivate();
	}
	
	public void Deactivate()
	{
		activated = false;
	}
	
	public bool IsActivated()
	{
		return activated;
	}
	
	public Vector3 GetPosition()
	{
		return this.transform.position;
	}
}
